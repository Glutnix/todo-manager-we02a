$(document).ready(function() {

	$('#add').click(function() {
		$('#add-item-form').show();
		$('#add-item-content').val('');
	});

	$('#add-item-form').submit(function(evt) {
		evt.preventDefault();
		var content = $('#add-item-content').val();

		var newItem = $('#new-item').html();
		newItem = $(newItem);

		newItem.find('span').text(content);
		$('#list').append(newItem);
		$(this).hide();
	});

	$('#cleanup').click(function() {
		$('#list .done').remove();
	});

	// hey #list, when a .remove inside of an li inside of you is clicked, run this function
	$('#list').on('click', 'li .remove', function(evt) {
		$(this).closest('li').remove();
		evt.stopPropagation();
	});

	$('#list').on('click', 'li', function() {
		$(this).toggleClass('done');
	});
});
